import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import nl.adaptivity.xmlutil.serialization.XML
import java.io.*

@Serializable
data class Restaurants(
    val address: Address,
    val borough: String,
    val cuisine: String,
    val grades: List<Grade>,
    val name: String,
    val restaurant_id: String
) : java.io.Serializable

@Serializable
data class Address(
    val building: String,
    val coord: List<Double>,
    val street: String,
    var zipcode: String
) : java.io.Serializable

@Serializable
data class Grade(
    val date: String,
    val mark: String,
    val score: Int
) : java.io.Serializable

fun main() {
    exercici2() // Json a Dat
    exercici3() // Dat a Xml
    exercici4() // Leer nombres de restaurantes
    exercici5() // Añadir nuevos restaurantes a Xml
    exercici6() // Cambiar zipCode de restaurante en Xml
}

//EJERCICIO 2: .Json -> .Dat
fun exercici2() {
    //Serializar
    val jsonFile = File("src/main/kotlin/restaurants.json")
    val jsonString = jsonFile.readText() //Leer el archivo .Json y guardarlo en String
    val json = Json.decodeFromString<List<Restaurants>>(jsonString) //Deserializar el .Json y guardarlo en una lista de Clases
    //Deserializar
    val datFile = ObjectOutputStream(FileOutputStream("src/main/kotlin/restaurants.dat"))
    datFile.writeObject(json) //Guardar el contenido del .Json en el .Dat
    datFile.close()
}

//EJERCICIO 3: .Dat -> .Xml
fun exercici3() {
    //Serializar
    val datFile = ObjectInputStream(FileInputStream("src/main/kotlin/restaurants.dat"))
    val datString = datFile.readObject() as List<Restaurants>
    datFile.close()
    //Deserializar
    val xml = XML.encodeToString(datString)
    val xmlFile = File("src/main/kotlin/restaurants.xml")
    xmlFile.writeText(xml)
}

//EJERCICIO 4: Leer nombres de los restaurantes
fun exercici4() {
    //Serializar
    val xmlFile = ObjectInputStream(FileInputStream("src/main/kotlin/restaurants.dat"))
    val xmlString = xmlFile.readObject() as List<Restaurants>
    xmlFile.close()
    //Imprimir nombres de restaurantes
    for (restaurant in xmlString) {
        println(restaurant.name)
    }
}

//EJERCICIO 5: Añadir restaurantes al .Xml
fun exercici5(){
    //Serializar XML
    val xmlFile = File("src/main/kotlin/restaurants.xml")
    val xmlString = xmlFile.readText()
    val xmlList = XML.decodeFromString<MutableList<Restaurants>>(xmlString)
    //Crear nuevos restaurantes
    val newRestaurants = listOf(
        Restaurants(Address("1254", listOf(-73.961704, 40.662942), "Rambla prim", "08019"),"Barcelona", "Spanish", listOf(Grade("1990-05-01", "F", 5), Grade("1991-04-22", "A", 8)), "Restsaurante Paco","96653554"),
        Restaurants(Address("1484", listOf(-93.926704, 20.551932), "Barceloneta", "08020"), "Barcelona", "Italian", listOf(Grade("1994-05-01", "F", 5), Grade("2001-02-02", "C", 7)), "Restsaurante ", "94857324"),
        Restaurants(Address("1002", listOf(-43.961458, 16.120214), "Plaza Cataluña", "08021"), "Barcelona", "Turkish", listOf(Grade("1992-07-14", "A", 9), Grade("1992-05-08", "A", 10)), "Kebabk", "356254785"))
    //Añadir los restaurantes nuevos a la lista de restaurantes
    for (restaurant in newRestaurants) {
        xmlList.add(restaurant) }
    //Deserializar
    val xml = XML.encodeToString(xmlList)
    File("src/main/kotlin/restaurants.xml").writeText(xml)
    }

//EJERCICIO 6: Cambiar zip code del ID 30075445
fun exercici6(){
    //Serializar
    val xmlFile = File("src/main/kotlin/restaurants.xml")
    val xmlString = xmlFile.readText()
    val xmlList = XML.decodeFromString<MutableList<Restaurants>>(xmlString)
    //Reemplazar CP si el ID = 30075445
    for (restaurant in xmlList) {
        if (restaurant.restaurant_id == "30075445") restaurant.address.zipcode = "10470"
    }
    //Deserializar
    val xml = XML.encodeToString(xmlList)
    File("src/main/kotlin/restaurants.xml").writeText(xml)
}